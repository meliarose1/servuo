﻿using System;
using System.Collections.Generic;

namespace Server.Gumps
{
    public abstract class Gumpling : IGumpContainer, IGumpComponent
    {
        private int _X = 0;
        private int _Y = 0;

        public int X
        {
            get { return _X; }
            set
            {
                int offset = value - X;
                _X = value;

                foreach (GumpEntry g in _Entries)
                    g.X += offset;
            }
        }

        public int Y
        {
            get { return _Y; }
            set
            {
                int offset = value - Y;
                _Y = value;

                foreach (GumpEntry g in _Entries)
                    g.Y += offset;
            }
        }

        private readonly List<IGumpComponent> _Entries;

        private IGumpContainer _MContainer;

        public IGumpContainer Container
        {
            get { return this._MContainer; }
            set
            {
                if (this._MContainer != value)
                {
                    if (this._MContainer != null)
                        this._MContainer.Remove(this);

                    this._MContainer = value;

                    if (this._MContainer != null)
                        this._MContainer.Add(this);
                }
            }
        }

        public Gumpling(int x, int y)
        {
            _X = x;
            _Y = y;

            this._Entries = new List<IGumpComponent>();
        }

        public Gump RootParent { get { return Container.RootParent; } }

        public void Add(IGumpComponent g)
        {
            if (g.Container == null)
                g.Container = this;

            if (!this._Entries.Contains((IGumpComponent)g))
            {
                g.X += _X;
                g.Y += _Y;

                this._Entries.Add((IGumpComponent)g);
                this.Invalidate();
            }
        }

        public void Remove(IGumpComponent g)
        {
            this._Entries.Remove(g);
            g.Container = null;
            this.Invalidate();
        }

        public virtual void Invalidate()
        {
        }

        public void AddToGump(Gump gump)
        {
            foreach (IGumpComponent g in _Entries)
                gump.Add(g);
        }

        public void RemoveFromGump(Gump gump)
        {
            foreach (IGumpComponent g in _Entries)
                gump.Remove(g);
        }
    }
}